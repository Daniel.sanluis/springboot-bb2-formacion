package com.formacion.practica;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.enums.StateEnum;
import com.formacion.practica.services.item.ItemService;
import com.formacion.practica.services.priceReduction.PriceReductionService;
import com.formacion.practica.services.role.RoleService;

import com.formacion.practica.services.supplier.SupplierService;
import com.formacion.practica.services.user.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc(addFilters = false)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PracticaApplicationTests {

	@Autowired
	RoleService roleService;

	@Autowired
	UserService userService;

	@Autowired
	PriceReductionService priceReductionService;

	@Autowired
	SupplierService supplierService;

	@Autowired
	ItemService	itemService;

	@Test
	@Order(1)
	void findRoles(){
		Assertions.assertThat(roleService.findAll()).hasSize(2);
	}

	@Test
	@Order(2)
	void findUser(){
		Assertions.assertThat(userService.findAll()).hasSize(2);
	}

	@Test
	@Order(3)
	void findPriceReductions(){
		Assertions.assertThat(priceReductionService.findAll()).hasSize(5);
	}

	@Test
	@Order(4)
	void findSupplers(){
		Assertions.assertThat(supplierService.findAll()).hasSize(5);
	}

	@Test
	@Order(5)
	void insertItem(){
		ItemDto itemDto = new ItemDto();
		itemDto.setState(StateEnum.ACTIVE);
		itemDto.setPrice(50D);
		itemDto.setDescription("Prueba");
		Assertions.assertThat(itemService.saveItem(itemDto,1l));
	}

	@Test
	@Order(6)
	void deleteUser(){
		Assertions.assertThat(userService.deleteUser(1l));
	}

	@Test
	@Order(7)
	void findUserById(){
		Assertions.assertThat(userService.findByIdUser(2l));
	}
}
