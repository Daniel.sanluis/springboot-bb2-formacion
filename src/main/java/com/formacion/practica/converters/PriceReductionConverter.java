package com.formacion.practica.converters;


import com.formacion.practica.dto.PriceReductionDto;

import com.formacion.practica.model.PriceReduction;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class PriceReductionConverter implements Converter<PriceReduction, PriceReductionDto> {
    @Override
    public PriceReductionDto convert(PriceReduction source) {
        ModelMapper modelMapper = new ModelMapper();
        PriceReductionDto priceReductionDto = modelMapper.map(source, PriceReductionDto.class);

        return priceReductionDto;
    }
}
