package com.formacion.practica.converters;

import com.formacion.practica.dto.RoleDto;
import com.formacion.practica.dto.UserDto;
import com.formacion.practica.model.Role;
import com.formacion.practica.model.User;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class UserDtoConvert implements Converter<UserDto, User> {
    @Override
    public User convert(UserDto source) {
        User user = new User();
        user.setIdUser(source.getIdUser());
        user.setUsername(source.getUsername());
        user.setPassword(source.getPassword());
        user.setEnabled(source.getEnabled());

        if(source.getRoles() != null){
            List<Role> roleList = new ArrayList<>();
            for (RoleDto roleDto: source.getRoles()){
                Role role = new Role();
                role.setIdRole(roleDto.getIdRole());
                role.setRole(roleDto.getRole());
                role.setDescription(roleDto.getDescription());
                roleList.add(role);
            }
            user.setRoles(roleList);
        }

        return user;
    }
}
