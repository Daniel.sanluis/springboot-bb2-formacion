package com.formacion.practica.converters;

import com.formacion.practica.dto.PriceReductionDto;
import com.formacion.practica.model.PriceReduction;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class PriceReductionDtoConverter implements Converter<PriceReductionDto, PriceReduction> {
    @Override
    public PriceReduction convert(PriceReductionDto source) {
        ModelMapper modelMapper = new ModelMapper();
        PriceReduction priceReduction = modelMapper.map(source, PriceReduction.class);

        return priceReduction;
    }
}