package com.formacion.practica.converters;

import com.formacion.practica.dto.SupplierDto;
import com.formacion.practica.model.Supplier;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

public class SupplierConverter implements Converter<Supplier, SupplierDto> {

    @Autowired
    ConversionService conversionService;

    @Override
    public SupplierDto convert(Supplier source) {
        ModelMapper modelMapper = new ModelMapper();
        SupplierDto supplierDto = modelMapper.map(source, SupplierDto.class);
        return supplierDto;
    }


}
