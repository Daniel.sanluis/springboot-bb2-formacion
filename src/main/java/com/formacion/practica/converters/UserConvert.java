package com.formacion.practica.converters;

import com.formacion.practica.dto.RoleDto;
import com.formacion.practica.dto.UserDto;
import com.formacion.practica.model.Role;
import com.formacion.practica.model.User;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class UserConvert implements Converter<User, UserDto> {
    @Override
    public UserDto convert(User source) {
        UserDto userDto = new UserDto();
        userDto.setIdUser(source.getIdUser());
        userDto.setUsername(source.getUsername());
        userDto.setPassword(source.getPassword());
        userDto.setEnabled(source.getEnabled());

        List<RoleDto> roleDtoList = new ArrayList<>();
        for (Role role: source.getRoles()){
            RoleDto roleDto = new RoleDto();
            roleDto.setIdRole(role.getIdRole());
            roleDto.setRole(role.getRole());
            roleDto.setDescription(role.getDescription());
            roleDtoList.add(roleDto);
        }
        userDto.setRoles(roleDtoList);

        return userDto;
    }
}
