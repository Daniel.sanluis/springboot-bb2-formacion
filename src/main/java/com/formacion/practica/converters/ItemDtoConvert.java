package com.formacion.practica.converters;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.model.Item;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class ItemDtoConvert implements Converter<ItemDto, Item> {
    @Override
    public Item convert(ItemDto source) {
        ModelMapper modelMapper = new ModelMapper();
        Item item = modelMapper.map(source, Item.class);
        return item;
    }
}
