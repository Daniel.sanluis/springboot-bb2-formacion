package com.formacion.practica.messages;

public class MessagesInfo {

    //EXCEPTIONS MESSAGES
    public static final String CONTRAINT_VIOLATION   = "The code cannot be equal to an existing one";
    public static final String NOT_CONTENT   = "There is no content in the search";
    public static final String NOT_EXIST_ITEM = "The item you want to delete does not exist";
    public static final String NOT_EXIST_USER = "The user you want to delete does not exist";
    public static final String NOT_NULL ="You are trying to save with properties that cannot be null";
    public static final String DATE_FAIL ="Dates cannot be overlapping";
}
