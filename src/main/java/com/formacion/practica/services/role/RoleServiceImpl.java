package com.formacion.practica.services.role;

import com.formacion.practica.dao.role.RoleRepository;
import com.formacion.practica.dto.RoleDto;
import com.formacion.practica.exceptions.NoContentException;
import com.formacion.practica.messages.MessagesInfo;
import com.formacion.practica.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService{

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ConversionService conversionService;

    @Override
    public List<RoleDto> findAll() {
        List<Role> roles = roleRepository.findAll();
        if(roles.isEmpty() || roles == null){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        List<RoleDto> rolesDto = roles.stream()
                .map(e -> conversionService.convert(e, RoleDto.class))
                .collect(Collectors.toList());
        return rolesDto;
    }
}
