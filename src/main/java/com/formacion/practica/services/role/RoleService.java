package com.formacion.practica.services.role;

import com.formacion.practica.dto.RoleDto;
import com.formacion.practica.dto.UserDto;

import java.util.List;

public interface RoleService {
    public List<RoleDto> findAll();
}
