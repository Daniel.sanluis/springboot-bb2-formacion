package com.formacion.practica.services.item;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.model.Item;

import java.util.List;

public interface ItemService {
    public List<ItemDto> findAll();
    public ItemDto findByIdItem(Long id);
    public boolean deleteItem(Long id);
    public boolean updateItem(ItemDto itemDto);
    public ItemDto saveItem(ItemDto itemDto, Long idUser);
}
