package com.formacion.practica.services.item;

import com.formacion.practica.dao.item.ItemRepository;
import com.formacion.practica.dao.pricereduction.PriceReductionRepository;
import com.formacion.practica.dao.user.UserRepository;
import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.dto.PriceReductionDto;
import com.formacion.practica.exceptions.BadRequestException;
import com.formacion.practica.exceptions.NoContentException;
import com.formacion.practica.messages.MessagesInfo;
import com.formacion.practica.model.Item;
import com.formacion.practica.model.PriceReduction;
import com.formacion.practica.model.User;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PriceReductionRepository priceReductionRepository;

    @Autowired
    ConversionService conversionService;

    //Devuelve una lista de todos los artículos
    @Override
    public List<ItemDto> findAll() {
        List<Item> items = itemRepository.findAll();
        if(items.isEmpty() || items == null){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        List<ItemDto> itemsDto = items.stream()
                .map(e -> conversionService.convert(e, ItemDto.class))
                .collect(Collectors.toList());
        return itemsDto;

    }

    @Override
    public ItemDto findByIdItem(Long id){
        if(itemRepository.findById(id) == null){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        return conversionService.convert(itemRepository.findById(id), ItemDto.class);
    }

    @Override
    public boolean deleteItem(Long id) {
        if (itemRepository.findById(id) != null) {
            Item item = itemRepository.findById(id).get();
            for (PriceReduction priceReduction: item.getPriceReductions()) {
                priceReductionRepository.deleteById(priceReduction.getIdPriceReduction());
            }
            itemRepository.delete(item);
            return true;
        }else {
            throw new BadRequestException(MessagesInfo.NOT_EXIST_ITEM);
        }
    }

    @Override
    public boolean updateItem(ItemDto itemDto) {
        if(itemRepository.findById(itemDto.getIdItem()) != null){
            if(!checkDate(itemDto.getPriceReductions())){
                throw new BadRequestException(MessagesInfo.DATE_FAIL);
            }

            List<PriceReduction> priceReductions =  priceReductionRepository.findByItem(itemRepository.findById(itemDto.getIdItem()).get());

            for (PriceReductionDto priceReductionDto: itemDto.getPriceReductions()){
                for(PriceReduction priceReduction : priceReductions){
                    if(priceReduction.getIdPriceReduction() != null && priceReduction.getIdPriceReduction()
                            .equals(priceReductionDto.getIdPriceReduction())){
                        priceReduction.setReducedPrice(priceReductionDto.getReducedPrice());
                        priceReduction.setStartDate(priceReductionDto.getStartDate());
                        priceReduction.setEndDate(priceReductionDto.getEndDate());

                        priceReductionRepository.save(priceReduction);
                    }else {
                        PriceReduction priceReduction1 = new PriceReduction();
                        priceReduction1 = conversionService.convert(priceReductionDto, PriceReduction.class);
                        priceReduction1.setItem(itemRepository.findById(itemDto.getIdItem()).get());
                        priceReductionRepository.save(priceReduction1);
                    }
                }

            }
            Item item = conversionService.convert(itemDto, Item.class);
            for (PriceReduction aux: item.getPriceReductions()) {
                aux.setItem(item);
            }
            itemRepository.save(item);


            return true;
        }
        return false;
    }

    @Override
    public ItemDto saveItem(ItemDto itemDto , Long idUser) {
        itemDto.setCreationDate(LocalDate.now());
        if(itemDto.getIdItem() == null){
            List<PriceReduction> priceReductions = new ArrayList<>();
            User user = userRepository.findById(idUser).get();
            itemDto.setCreator(user.getUsername());

            Item item = conversionService.convert(itemDto, Item.class);
            if(user == null){
                throw new BadRequestException(MessagesInfo.NOT_EXIST_USER);
            }



            itemDto.setIdItem(item.getIdItem());

            if(itemDto.getPriceReductions() != null){
                for (PriceReductionDto priceReductionDto: itemDto.getPriceReductions()) {
                    if(!checkDate(itemDto.getPriceReductions())){
                        throw new BadRequestException(MessagesInfo.DATE_FAIL);
                    }
                    priceReductions.add(conversionService.convert(priceReductionDto, PriceReduction.class));
                }
                itemRepository.save(item);

                for (PriceReduction priceReduction: priceReductions) {
                    priceReduction.setItem(item);
                    priceReductionRepository.save(priceReduction);
                }
            }
            return itemDto;
        }
        return null;
    }

    private boolean checkDate(List<PriceReductionDto> priceReductions){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        for (PriceReductionDto priceReduction: priceReductions) {
            DateTime fechaInicio1 = formatter.parseDateTime(priceReduction.getStartDate().toString());
            DateTime fechaFin1 = formatter.parseDateTime(priceReduction.getEndDate().toString());
            Interval intervalo1 = new Interval( fechaInicio1, fechaFin1 );
            List<PriceReductionDto> filteredList =
                    priceReductions.stream().filter(p -> !priceReduction
                            .equals(p)).collect(Collectors.toList());
            for (PriceReductionDto filtered: filteredList) {
                DateTime fechaInicio2 = formatter.parseDateTime(filtered.getStartDate().toString());
                DateTime fechaFin2 = formatter.parseDateTime(filtered.getEndDate().toString());
                Interval intervalo2 = new Interval( fechaInicio2, fechaFin2 );
                if(intervalo1.overlaps( intervalo2 )){
                    return false;
                }
            }
        }
        return true;
    }
}


