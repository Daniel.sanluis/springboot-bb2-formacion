package com.formacion.practica.services.priceReduction;

import com.formacion.practica.dto.PriceReductionDto;

import java.util.List;

public interface PriceReductionService {
    public List<PriceReductionDto> findAll();

}
