package com.formacion.practica.services.priceReduction;

import com.formacion.practica.dao.pricereduction.PriceReductionRepository;
import com.formacion.practica.dto.PriceReductionDto;
import com.formacion.practica.exceptions.NoContentException;
import com.formacion.practica.messages.MessagesInfo;
import com.formacion.practica.model.PriceReduction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PriceReductionServiceImpl implements PriceReductionService{

    @Autowired
    PriceReductionRepository priceReductionRepository;

    @Autowired
    ConversionService conversionService;

    @Override
    public List<PriceReductionDto> findAll() {
        List<PriceReduction> pricereductions = priceReductionRepository.findAll();
        if(pricereductions.isEmpty() || pricereductions == null){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        List<PriceReductionDto> pricereductionsDto = pricereductions.stream()
                .map(e -> conversionService.convert(e, PriceReductionDto.class))
                .collect(Collectors.toList());
        return pricereductionsDto;
    }
}
