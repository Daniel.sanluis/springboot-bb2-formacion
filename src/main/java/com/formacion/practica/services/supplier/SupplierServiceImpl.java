package com.formacion.practica.services.supplier;

import com.formacion.practica.dao.supplier.SupplierRepository;
import com.formacion.practica.dto.SupplierDto;
import com.formacion.practica.exceptions.NoContentException;
import com.formacion.practica.messages.MessagesInfo;
import com.formacion.practica.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierServiceImpl implements SupplierService{

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ConversionService conversionService;

    @Override
    public List<SupplierDto> findAll() {
        List<Supplier> suppliers = supplierRepository.findAll();
        if(suppliers.isEmpty() || suppliers == null){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        List<SupplierDto> suppliersDto = suppliers.stream()
                .map(e -> conversionService.convert(e, SupplierDto.class))
                .collect(Collectors.toList());
        return suppliersDto;
    }
}
