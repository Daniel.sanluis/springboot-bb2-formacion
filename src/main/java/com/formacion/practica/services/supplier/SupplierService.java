package com.formacion.practica.services.supplier;

import com.formacion.practica.dto.SupplierDto;

import java.util.List;

public interface SupplierService {
    public List<SupplierDto> findAll();
}
