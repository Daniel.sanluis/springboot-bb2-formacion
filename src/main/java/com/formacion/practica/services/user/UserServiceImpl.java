package com.formacion.practica.services.user;

import com.formacion.practica.dao.user.UserRepository;
import com.formacion.practica.dto.UserDto;
import com.formacion.practica.exceptions.BadRequestException;
import com.formacion.practica.exceptions.NoContentException;
import com.formacion.practica.messages.MessagesInfo;
import com.formacion.practica.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    ConversionService conversionService;



    //Devuelve una lista de todos los artículos
    @Override
    public List<UserDto> findAll() {
        List<User> users = userRepository.findAll();
        if(users.isEmpty()){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        return users.stream()
                .map(e -> conversionService.convert(e, UserDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDto findByIdUser(Long id) {
        if(!userRepository.findById(id).isPresent()){
            throw new NoContentException(MessagesInfo.NOT_CONTENT + ".");
        }
        return conversionService.convert(userRepository.findById(id),UserDto.class) ;
    }

    @Override
    public boolean deleteUser(Long id) {
        if(!userRepository.findById(id).isPresent()){
            userRepository.deleteById(id);
            return true;
        }else {
            throw new BadRequestException(MessagesInfo.NOT_EXIST_USER);
        }
    }

    public boolean updateUser(UserDto userDto) {
        if(!userRepository.findById(userDto.getIdUser()).isPresent()){
            User user = conversionService.convert(userDto, User.class);
            if(user != null){

                userRepository.save(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public UserDto saveUser(UserDto userDto) {
        if(userDto.getIdUser() == null){
            User user = conversionService.convert(userDto, User.class);
            if(user != null){
                userRepository.save(user);
                userDto.setIdUser(user.getIdUser());
                return userDto;
            }
        }
        return null;
    }

    @Override
    public UserDto findByUsername(String username) {
       UserDto userDto = conversionService.convert(userRepository.findByUsername(username),UserDto.class);
       if(userDto != null){
           return userDto;
       }else
       {
           throw new BadRequestException(MessagesInfo.NOT_EXIST_USER);
       }
    }


}
