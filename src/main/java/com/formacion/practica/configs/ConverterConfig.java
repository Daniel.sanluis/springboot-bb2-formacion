package com.formacion.practica.configs;

import com.formacion.practica.converters.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ConverterConfig implements WebMvcConfigurer {

    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new ItemConverter());
        registry.addConverter(new ItemDtoConvert());
        registry.addConverter(new SupplierConverter());
        registry.addConverter(new PriceReductionConverter());
        registry.addConverter(new PriceReductionDtoConverter());
        registry.addConverter(new RoleConverter());
        registry.addConverter(new UserConvert());
        registry.addConverter(new UserDtoConvert());
    }

}
