package com.formacion.practica.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.formacion.practica.enums.StateEnum;
import com.formacion.practica.model.PriceReduction;
import lombok.Data;
import java.time.LocalDate;
import java.util.List;

@Data
public class ItemDto {

    private Long idItem;

    private Long itemCode;

    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
    private LocalDate creationDate;

    private Double price;

    private StateEnum State;

    private String creator;

    private String messageDisabled;

    private List<SupplierDto> suppliers;

    private List<PriceReductionDto> priceReductions;
}
