package com.formacion.practica.dto;

import com.formacion.practica.model.Role;
import lombok.Data;

import java.util.List;

@Data
public class UserDto {

    private Long idUser;

    private String username;

    private String password;

    private String token;

    private Boolean enabled;

    private List<RoleDto> roles;
}
