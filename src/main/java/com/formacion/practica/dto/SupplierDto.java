package com.formacion.practica.dto;

import com.formacion.practica.model.Item;
import lombok.Data;


import java.util.List;

@Data
public class SupplierDto {

    private Long idSupplier;

    private String name;

    private String country;

   // private List<ItemDto> items;
}
