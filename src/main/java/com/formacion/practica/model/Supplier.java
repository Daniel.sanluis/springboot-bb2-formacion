package com.formacion.practica.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="SUPPLIER")
@Data
public class Supplier {

    @Id
    @Column(name = "IDSUPPLIER")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "supplier_generator")
    @SequenceGenerator(name="supplier_generator",initialValue = 6, allocationSize = 100)
    private Long idSupplier;

    @Column(name="NAME")
    private String name;

    @Column(name="COUNTRY")
    private String country;

    @ManyToMany(mappedBy = "suppliers")
    private List<Item> items;
}
