package com.formacion.practica.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name="PRICEREDUCTION")
@Getter
@Setter
public class PriceReduction {

    @Id
    @Column(name = "IDPRICEREDUCTION")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "priceReduction_generator")
    @SequenceGenerator(name="priceReduction_generator",initialValue = 6, allocationSize = 100)
    private Long idPriceReduction;

    @Column(name="REDUCEDPRICE")
    private double reducedPrice;

    @Column(name="STARTDATE")
    private LocalDate startDate;

    @Column(name="ENDDATE")
    private LocalDate endDate;

    @ManyToOne(cascade = {CascadeType.ALL})
    private Item item;
}
