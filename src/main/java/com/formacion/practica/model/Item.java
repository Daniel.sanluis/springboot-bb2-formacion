package com.formacion.practica.model;

import com.formacion.practica.enums.StateEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Entity
@Table(name="ITEM")
@Getter
@Setter
public class Item {

    @Id
    @Column(name = "IDITEM")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_generator")
    @SequenceGenerator(name="item_generator",initialValue = 6, allocationSize = 100)
    private Long idItem;

    @Column(name="ITEMCODE",unique=true)
    private Long itemCode;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="CREATIONDATE")
    private LocalDate creationDate;

    @Column(name="PRICE")
    private Double price;

    @Column(name="STATE")
    private StateEnum State;

    @ManyToMany
    @JoinTable(
        name = "ITEMS_SUPPLIERS",
        joinColumns = @JoinColumn(name="IDITEM"), inverseJoinColumns = @JoinColumn(name = "IDSUPPLIER")
    )
    private List<Supplier> suppliers;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "item")
    private List<PriceReduction> priceReductions;

    private String creator;

    private String messageDisabled;

    //private price reduction:

}
