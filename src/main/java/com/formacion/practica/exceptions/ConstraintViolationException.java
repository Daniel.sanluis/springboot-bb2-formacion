package com.formacion.practica.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ConstraintViolationException extends RuntimeException {

    private static final String DESCRIPTION = "Constrain Violation Request Exception (400)";

    public ConstraintViolationException(String details) {
        super(DESCRIPTION + ". " + details);
    }

}

