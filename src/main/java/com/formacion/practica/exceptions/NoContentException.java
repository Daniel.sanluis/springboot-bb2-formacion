package com.formacion.practica.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class NoContentException extends RuntimeException {

    private static final String DESCRIPTION = "No content Exception (204)";

    public NoContentException(String details) {
        super(DESCRIPTION + ". " + details);
    }
}

