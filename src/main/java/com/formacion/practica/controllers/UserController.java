package com.formacion.practica.controllers;

import com.formacion.practica.dto.UserDto;
import com.formacion.practica.model.User;
import com.formacion.practica.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findUsers")
    public ResponseEntity<Object> getUsers(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/userid/{idUser}")
    public ResponseEntity<Object> getUserById(HttpServletRequest request, @PathVariable Long idUser) throws Exception{
        return new ResponseEntity<Object>(userService.findByIdUser(idUser), HttpStatus.OK);
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<Object> getUserById(HttpServletRequest request, @PathVariable String username) throws Exception{
        return new ResponseEntity<Object>(userService.findByUsername(username), HttpStatus.OK);
    }

    @DeleteMapping("/deleteUser/{idUser}")
    public boolean deleteUser(HttpServletRequest request, @PathVariable Long idUser) throws Exception{
        return userService.deleteUser(idUser);
    }

    @PostMapping("/saveUser")
    public ResponseEntity<Object> saveUser(HttpServletRequest request, @RequestBody UserDto user) throws Exception{
        return new ResponseEntity<Object>(userService.saveUser(user), HttpStatus.OK);
    }

    @PutMapping("/updateUser")
    public boolean updateUser(HttpServletRequest request, @RequestBody UserDto user) throws Exception{
        return userService.updateUser(user);
    }
}
