package com.formacion.practica.controllers;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.services.item.ItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/shop")
@Api(value = "Item service")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @ApiOperation(value = "Find Items", notes = "Return a Items List" )
    @GetMapping("/findItems")
    public ResponseEntity<Object> getItems(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(itemService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/item/{idItem}")
    public ResponseEntity<Object> getItemById(HttpServletRequest request, @PathVariable Long idItem) throws Exception{
        return new ResponseEntity<Object>(itemService.findByIdItem(idItem), HttpStatus.OK);
    }

    @DeleteMapping("/deleteItem/{idItem}")
    public boolean deleteItem(HttpServletRequest request, @PathVariable Long idItem) throws Exception{
        return itemService.deleteItem(idItem);
    }

    @PostMapping("/saveItem/{idUser}")
    public ResponseEntity<Object> saveItem(HttpServletRequest request, @RequestBody ItemDto item, @PathVariable Long idUser ) throws Exception{
        return new ResponseEntity<Object>(itemService.saveItem(item, idUser), HttpStatus.OK);
    }

    @PutMapping("/updateItem")
    public boolean updateItem(HttpServletRequest request, @RequestBody ItemDto item) throws Exception{
        return itemService.updateItem(item);
    }

}
