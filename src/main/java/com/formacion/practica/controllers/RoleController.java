package com.formacion.practica.controllers;

import com.formacion.practica.services.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping("/findRoles")
    public ResponseEntity<Object> getRoles(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(roleService.findAll(), HttpStatus.OK);
    }
}
