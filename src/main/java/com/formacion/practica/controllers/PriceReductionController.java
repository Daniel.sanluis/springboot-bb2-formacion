package com.formacion.practica.controllers;

import com.formacion.practica.services.priceReduction.PriceReductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping
public class PriceReductionController {

    @Autowired
    private PriceReductionService priceReductionService;

    @GetMapping("/findPriceReductions")
    public ResponseEntity<Object> getPriceReductions(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(priceReductionService.findAll(), HttpStatus.OK);
    }
}
