package com.formacion.practica.dao.pricereduction;

import com.formacion.practica.model.Item;
import com.formacion.practica.model.PriceReduction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriceReductionRepository extends JpaRepository<PriceReduction, Long> {
    public List<PriceReduction> findByItem(Item item);
}
