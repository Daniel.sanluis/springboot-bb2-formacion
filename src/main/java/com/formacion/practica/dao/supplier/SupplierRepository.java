package com.formacion.practica.dao.supplier;

import com.formacion.practica.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository  extends JpaRepository<Supplier, Long> {
}
