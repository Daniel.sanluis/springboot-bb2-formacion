package com.formacion.practica.dao.item;

import com.formacion.practica.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface ItemRepository extends JpaRepository<Item,Long> {

    @Query(
            value = "SELECT * FROM item " +
                    "             JOIN items_suppliers ON items_suppliers.iditem = item.iditem JOIN ( " +
                    "                   SELECT items_suppliers.idsupplier, MIN(item.price) pricemin " +
                    "                   FROM item " +
                    "                   JOIN items_suppliers ON items_suppliers.iditem = item.iditem " +
                    "                   GROUP BY items_suppliers.idsupplier " +
                    "             ) result " +
                    "             ON result.idsupplier = items_suppliers.idsupplier " +
                    "             AND result.pricemin = item.price;",
            nativeQuery = true)
    List<Item> findItemSupplierOrderPrice();

    @Query(
            value = "SELECT * FROM SUPPLIER inner join items_suppliers" +
                    " on items_suppliers.idsupplier = supplier.idsupplier " +
                    "inner join item on item.iditem = items_suppliers.iditem " +
                    "inner join pricereduction on pricereduction.item_iditem = item.iditem; ",
            nativeQuery = true)
    List<Item> findSupplierPriceReduction();
}
