# Formación BB2

_Api Rest Spring Boot, pequeño software para la gestión de mercancías en una empresa._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local._
 * [Clonar](https://gitlab.com/Daniel.sanluis/springboot-bb2-formacion.git)


## Construido con 🛠️

_Stack Tecnológico y librerías_
* [JAVA 8](https://www.java.com/es/download/help/java8.html) - Java 8 es la versión más reciente de Java que incluye nuevas características, mejoras y correcciones de bugs para mejorar la eficacia en el desarrollo y la ejecución de programas Java. .
* [Spring - Spring Boot 2.4.1](https://spring.io/blog/2020/12/11/spring-boot-2-4-1-available-now) - Spring es un framework para desarrollo de apliaciones Java. Spring Boot es una extesión, un suite, pre-configurado para ejecutar Spring fuera-de-la-caja utilizando las mejores prácticas y sin perder flexibilidad.
* [H2-DATABASE](https://www.h2database.com/html/main.html) - H2 es un sistema administrador de bases de datos relacionales programado en Java. Puede ser incorporado en aplicaciones Java o ejecutarse de modo cliente-servidor.
* [MODEL-MAPPER](http://modelmapper.org/) - Facilita el mapeo de objetos, determinando automáticamente cómo un modelo de objeto se asigna a otro, según las convenciones, de la misma manera que lo haría un humano, al tiempo que proporciona una API simple y segura para la refactorización para manejar casos de uso específicos.
* [JWT](https://jwt.io/) - Es un estándar abierto basado en JSON propuesto por IETF para la creación de tokens de acceso que permiten la propagación de identidad y privilegios o claims en inglés.
* [Swagger](https://swagger.io/) - Es un conjunto de herramientas de software de código abierto para diseñar, construir, documentar, y utilizar servicios web RESTful. 
* [Joda-Time](https://www.joda.org/joda-time/) - Permite trabajar con fechas de una forma más sencilla, potente y eficiente que el API estándar de fechas de Java.


## Autores ✒️

* **Daniel San Luis Acosta** 
